<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
	{
		return view('index');
	}
	
	public function table()
	{
		return view('table');
	}
	
	public function datatables()
	{
		return view('data-table');
	}
}
