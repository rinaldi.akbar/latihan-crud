@extends('app')
@section('content')
<div>
    <h2>Edit Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
			@method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
			<div class="form-group">
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" value="{{$cast->umur}}">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" value="{{$cast->bio}}">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection